from flask import Blueprint, render_template
import os

frontend_app = Blueprint('frontend_app', __name__,
    template_folder='templates',
    static_folder='static',
    static_url_path='/frontend/static'
)

@frontend_app.route("/")
def index():
    google_tag = os.getenv('google_tag')
    google_adsense = os.getenv('google_adsense')
    return render_template("index.html", google_tag=google_tag, google_adsense=google_adsense)
