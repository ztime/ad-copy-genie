const api_endpoint = '/v1/'

const vm = new Vue({
	el: "#vm",
	delimiters: [ '[[', ']]' ],
	data: {
    loading: false,
    loaded_title: '',
    loaded_text_ad: '',
    loaded_video_script: '',
    response_is_loaded_once: false,
    description: '',
    language: 'Swedish'
	},
  methods: {
    get_ads: function() {
      this.loading = true
      const requestOptions = {
        method: "POST",
        headers: {"Content-Type": "application/x-www-form-urlencoded"},
        body: new URLSearchParams({
          'language': language.value,
          'description': description.value
        })
      }
      fetch( api_endpoint + 'get_ads', requestOptions)
        .then(response => {
          return response.json()
        })
        .then(({ad,title,video_script}) => {
          this.loaded_title = title
          this.loaded_text_ad = ad
          this.loaded_video_script = video_script
          this.response_is_loaded_once = true
          this.loading = false
        })
    },
    set_selected_field: function(selected_field) {
      this.selected_field = selected_field
    }
  }
})
