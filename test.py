import openai
import os

openai.api_key = os.getenv("OPENAI_API_KEY")

prompt = """Create unique decorative room dividers for modern interiors with our solid wood frames and double-sided prints. Ideal for apartments, businesses, and even healthcare settings, these practical and stylish dividers can be used to separate living and sleeping spaces, hide laundry, or even create a dressing room. With a variety of sizes available, these decorative dividers are perfect for any room in your home or office."""

response = openai.Image.create(
    prompt=prompt,
    n=1,
    size="1024x1024"
)
print(response)
print(response['data'][0]['url'])

