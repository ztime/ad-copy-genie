from flask import Blueprint, request
import openai
import os

openai.api_key = os.getenv("OPENAI_API_KEY")

USE_OPENAI = False
if os.getenv("USE_OPENAI_API") == 'use_openai':
    USE_OPENAI = True

backend_app = Blueprint('backend_app', __name__)

@backend_app.route("/get_ads", methods=["POST"])
def get_ads():
    print('Starting call to open ai')
    description = request.form['description']
    language = request.form['language']
    if not USE_OPENAI:
        return mocked_response

    messages=[]
    messages.append({'role':'system','content':"you are a helpful assistant that writes titles and ads for a products based on it's description"})
    # instructions = f"""Write a title optimized for google shopping in {language} following this format:
# Title: 
# Write a text for google search ads in {language} following this format:
# Text ad:
# Write a script for a video ad 15 seconds long in {language} following this format:
# Video script:"""
    instructions = f"""Write a title optimized for google shopping in {language} following this format:
Title: this is an example title
Write a text for google search ads in {language} following this format:
Text ad: this is an example text ad 
Write a script for a video ad 15 seconds long in {language} following this format:
Video script:
This is a multiline example of a video script
it can go on for many lines"""
    messages.append({'role':'user','content':f'This is the product description "{description}" in {language}. {instructions}'})

    response = openai.ChatCompletion.create(
      model="gpt-3.5-turbo",
      messages=messages,
      max_tokens=1000,
    )
    print(response)
    response_text = response.choices[0].message.content
    print(response_text)
    title,ad,video_script = extract_ads_from_text(response_text)
    return { 'title': title, 'ad': ad, 'video_script':video_script }

def extract_ads_from_text(raw_text):
    title = None
    ad = None
    video_script_lines = []
    parsing_script = False
    for line in raw_text.splitlines():
        if parsing_script:
            video_script_lines.append(line)
        elif line.startswith('Title'):
            title = line.split(':', 1)[1]
        elif line.startswith('Text ad:'):
            ad = line.split(':', 1)[1]
        elif line.startswith("Video script"):
            parsing_script = True
            video_script_lines.append(line.split(':', 1)[1])
    video_script = '\n'.join(video_script_lines)
    return title,ad,video_script


mocked_data = """Title for Google Shopping: Köp Tiki CS265-RB Släpvagn med Kåpa - 750 kg eller 1000 kg

Text for Google Search Ads: Köp Tiki CS265-RB Släpvagn med Kåpa online. Välj mellan totalvikt 750 kg eller 1000 kg. Lastförmåga Maxlast 518 / 767 kg. Varmgalvaniserad stålkonstruktion och tippbart flak. Perfekt för transport av stora föremål. Beställ nu!

Video Ad Script:
[Scene: A person struggling to fit large items in their car]
Person: Åh nej, inget får plats i bilen!
[Scene: The Tiki CS265-RB trailer with the Jaxalkåpa]
Narrator: Har du problem med att transportera stora saker?
[Scene: The person hooks up the trailer to their car]
Narrator: Med Tiki CS265-RB Släpvagn med Kåpa kan du enkelt transportera allt du behöver!
[Scene: The person drives off with the trailer]
Narrator: Välj mellan totalvikt 750 kg eller 1000 kg. Tippbart flak, starka lämmar och invändiga bindningsöglor gör lastningen superenkelt.
[Scene: The person arrives at their destination and unloads the trailer]
Narrator: Beställ Tiki CS265-RB Släpvagn med Kåpa online idag och ta itu med alla dina transportbehov på ett enkelt sätt!"""

mocked_description = """Produktbeskrivning I paketet ingår bromsade släpvagnen Tiki CS265-RB med 100 cm hög Jaxalkåpa, stödhjul och lås. Välj mellan totalvikt 750 kg eller 1000 kg. Lastförmåga Maxlast 518 / 767 kg Lastutrymme (LxB) 265 x 125 cm Lämhöjd 33 cm Höjd kåpa 100 cm Total innerhöjd 133 cm Mått och vikt på släpet Totalvikt 750 / 1000 kg Tjänstevikt 232 kg Totallängd 399 cm Totalbredd 170 cm Axeltyp Enkelaxel gummifjädring Hjuldimension 13 tum, 4 bult stålfälg Övrig info om släpet Varmgalvaniserad stålkonstruktion Invändiga bindningsöglor Utvändiga bindningskrokar 13-polig el-kontakt Tippbart flak Flakbotten av vattenbeständig plywood Starka lämmar - fäll och löstagbara Stödhjul ingår Släpet på bilden kan vara extrautrustat. Avhämtningsort Tiki kan avhämtas utan fraktkostnad i följande orter Askersund Håkantorp Gralslund 316, Askersund (Koordinater 58.8520:14.7868) Borgeby Tubavägen 1, Bjärred (Koordinater 55.73876:13.03470) Stockholm Finkmossvägen 52, Tumba (Koordinater 59.175737:17.816402) Östanå Osbyvägen, Östanå (Koordinater 56.313561:14.022156) Alternativt för fraktkostnad 4000 kr + moms till följande orter Umeå (Lamellgatan 1, Umeå Koordinater 63.80815:20.27277) Föllinge (Tunvägen 16, Föllinge Koordinater 63.67022:14.60313) Frakt till schenkerdepå Alternativt leverans med Schenker. För att se fraktpriset, lägg släpvagnen i varukorgen. Gå sedan till kassan och välj önskad Schenkerdepå."""

mocked_response = {
  "ad": " Köp vår bromsade släpvagn Tiki CS265-RB med Jaxalkåpa, stödhjul och lås för säker och smidig transport av dina varor. Välj mellan totalvikt 750 kg eller 1000 kg med en maxlastkapacitet på upp till 767 kg. Med tippbart flak och invändiga bindningsöglor är den perfekt för både stora och små transportbehov. Beställ online nu och hämta gratis i en av våra avhämtningsorter.",
  "title": " Släpvagn med Jaxalkåpa, stödhjul och lås",
  "video_script": " \nVisa upp en bild av släpvagnen från olika vinklar medan en voice-over säger:\n\"Behöver du en smidig lösning för att transportera dina varor? Titta på vår tippbara släpvagn Tiki CS265-RB med Jaxalkåpa, stödhjul och lås. Välj mellan totalvikt 750 kg eller 1000 kg och njut av dess maxlastkapacitet på upp till 767 kg. Med invändiga bindningsöglor och tippbart flak kan du enkelt transportera allt från små till stora föremål. Beställ nu online och hämta gratis i en av våra avhämtningsorter.\""
}
